import subprocess
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--nSTA', type=int, required=True, 
    help="number of stations for this scenario")
    parser.add_argument('--initialCWmin', type=int, default=15, 
    help="Initial value of CW min")
    parser.add_argument('--cwMinLimit', type=int, required=True, 
    help="Limit to CW value iterator")
    parser.add_argument('--seedValue', type=int, default=0, 
    help="Seed value provided to simulator. Default 0 -> random seed value")
    parser.add_argument('--simTime', type=int, default=300, 
    help="Simulation time in seconds")
    parser.add_argument('--Mbps', type=int, default=54, 
    help="Load value in Mbps. Default is 54 Mbps")
    parser.add_argument('--stepSize', type=int, default=16, 
    help="Step size for CW min value")
    

    
    

    args = parser.parse_args()

    try:
        x = args.initialCWmin
        seed = args.seedValue
        simTime = args.simTime
        stepSize = args.stepSize
        Mbps = args.Mbps
        print("Initial arguments:")
        print("\tnSTA: {}".format(args.nSTA))
        print("\tinitialCWmin: {}".format(args.initialCWmin))
        print("\tcwMinLimit: {}".format(args.cwMinLimit))
        print("\tseedValue: {}".format(args.seedValue))
        print("\tsimTime: {}".format(args.simTime))
        print("\tstepSize: {}".format(stepSize))
        print("\tMbps: {}".format(Mbps))

        while x <= args.cwMinLimit:

            command = "cd ~/source/ns-3.29 ; ./waf --run \"scratch/szymon_scenario_80211_a \
                                                            --VO=0 --VI=0 --BE=1 --BK=0 \
                                                            --Mbps={} --packetSize=1470 --calcStart=10 \
                                                            --simTime={} --nSTA={} --anomCWmin={} --seed={}\"".format(
                                                                Mbps, simTime, args.nSTA, x, seed)
            print("Starting command : ", command)
            process = subprocess.run(command, shell=True, stdout=subprocess.PIPE)
            # process.wait()
            print(process.returncode)
            
            # x *= 2
            x += stepSize

    except Exception as e:
        print(e)
        exit(1)
