/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 AGH University of Science and Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as 
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Lukasz Prasnal <prasnal@kt.agh.edu.pl>
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/propagation-module.h"
#include "ns3/mobility-module.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"

#include <algorithm> 
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <numeric>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <mutex>
#include <bits/stdc++.h> 
#include <sys/stat.h> 
#include <sys/types.h> 


using namespace ns3; 

NS_LOG_COMPONENT_DEFINE ("wifi-qos-test");

std::mutex logMutex;

typedef std::map<uint64_t, Time> Packets;
uint64_t m_lastFrameUID;
const uint32_t nSTA = 1;
float calcStart = 0;
float simTime = 10;
uint32_t anomCWmin = 15;

struct StationInformation
{

  Ptr<Node> staNode;
  Mac48Address staMacAddress;
  Ipv4Address staAddress;

  uint32_t msdusOrig;
  uint32_t msdusTxed,     msdusRxed;
  uint64_t sendBytes,     recvBytes;
  uint32_t overloadDrops, lifetimeDrops; 
  uint32_t msduTxFails,   amsduTxFails;
  uint32_t msdusRetry,    amsdusRetry;
  uint32_t amsdusTxed,    amsdusRxed;
  uint32_t ampdusTxed,    ampdusRxed;
  uint32_t rxErrors,      ackRxErrors;
  Time     delaySum,      jitterSum;


  Packets  m_sentFrames;
  Time     m_lastDelay;
  uint8_t  m_prevPreambleTxed, m_prevPreambleRxed;
  uint8_t  m_lastFrameTID;

  double Throughput, Mean_Delay, Mean_Jitter;




  uint32_t L2msdusOrig;
  uint32_t L2msdusTxed,     L2msdusRxed;
  uint64_t L2sendBytes,     L2recvBytes;
  uint32_t L2overloadDrops, L2lifetimeDrops; 
  uint32_t L2msduTxFails,   L2amsduTxFails;
  uint32_t L2msdusRetry,    L2amsdusRetry;
  uint32_t L2amsdusTxed,    L2amsdusRxed;
  uint32_t L2ampdusTxed,    L2ampdusRxed;
  uint32_t L2rxErrors,      L2ackRxErrors;
  Time     L2delaySum,      L2jitterSum;


  Packets  L2m_sentFrames;
  Time     L2m_lastDelay;
  uint8_t  L2m_prevPreambleTxed, L2m_prevPreambleRxed;
  uint8_t  L2m_lastFrameTID;
  uint64_t L2m_lastACKFrameUID, L2m_lastDATAFrameUID;

};

std::vector<StationInformation> stationInfoVec;

bool fileExists(std::string& fileName) {
    return static_cast<bool>(std::ifstream(fileName));
}

bool writeCsvHeader(std::string &fileName, std::vector<std::string> columns, int staNumber) {
    std::lock_guard<std::mutex> csvLock(logMutex);
    std::fstream file;
    file.open (fileName, std::ios::out | std::ios::app);
    if (file) {
        for(uint32_t n=0; n <columns.size(); n++){
          file << "\"" << columns[n] << "\",";
        }
        for(uint32_t m=0; m < (unsigned)staNumber; m++){
          file << "\"" << "Station " << m << " Throughput(Mbps)" << "\",";
          file << "\"" << "Station " << m << " Channel Occupancy(s)" << "\",";
        }
        file <<  std::endl;
        return true;
    } else {
        return false;
    }
}

bool writeCsvFile(std::string &fileName, std::vector<StationInformation> staInfoVec, int anomIndex) {
    std::lock_guard<std::mutex> csvLock(logMutex);
    std::fstream file;
    file.open (fileName, std::ios::out | std::ios::app);

    if (file) {
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [80];
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(buffer,sizeof(buffer),"%Y-%m-%d %H:%M:%S",timeinfo);
        std::string t_str(buffer);

        file << "\"" << t_str << "\",";
        file << "\"" << anomCWmin << "\",";
        file << "\"" << calcStart << "\",";
        file << "\"" << simTime << "\",";

        double throughputFast = 0;
        double occupancyFast = 0;
        for(uint32_t n=anomIndex+1; n < staInfoVec.size(); n++){
          throughputFast += staInfoVec[n].Throughput;
          occupancyFast += staInfoVec[n].L2delaySum.GetSeconds();
        }

        file << "\"" << throughputFast << "\",";
        file << "\"" << occupancyFast << "\",";
        file << "\"" << occupancyFast/(staInfoVec.size()-nSTA*2) << "\",";

        file << "\"" << staInfoVec[anomIndex].Throughput << "\",";
        file << "\"" << staInfoVec[anomIndex].L2delaySum.GetSeconds() << "\",";

        for(uint32_t m=0; m < (unsigned)staInfoVec.size(); m++){
          file << "\"" << staInfoVec[m].Throughput << "\",";
          file << "\"" << staInfoVec[m].L2delaySum.GetSeconds()<< "\",";
        }

        file << std::endl;

        return true;
    } else {
        return false;
    }
}

void writeStatisticData(std::string directory, std::string filename, int anomIndex)
{
  std::string my_command ="mkdir -p "+directory; 
  const char *command = my_command.c_str();
  system(command);

  std::string fullpath = directory + "/" + filename;

  if(!fileExists(fullpath))
  {
    std::vector<std::string> columns {"Timestamp", 
                                      "CW min anomaly",
                                      "Warmup Time(s)",
                                      "Simulation Time(s)",
                                      "Throughput sum for fast stations(Mbps)",
                                      "Channel Occupancy sum for fast stations(s)",
                                      "Channel Occupancy mean for fast stations(s)",
                                      "Throughput for anomaly station(Mbps)",
                                      "Channel Occupancy for anomaly stations(s)"};
    writeCsvHeader(fullpath, columns, stationInfoVec.size());
  }

  writeCsvFile(fullpath, stationInfoVec, anomIndex);
}



void GetCw()
{

  std::cout << "Checking CW values for nodes:" << std::endl;
  for(uint32_t n=0; n < NodeList::GetNNodes(); n++){
    Ptr<NetDevice> dev = NodeList::GetNode (n)->GetDevice (0);
    Mac48Address tmpAddress = Mac48Address::ConvertFrom (dev->GetAddress ());

    Ptr<WifiNetDevice> wifi_dev = DynamicCast<WifiNetDevice> (dev);
    Ptr<WifiMac> wifi_mac = wifi_dev->GetMac ();
    Ptr<RegularWifiMac> rmac = DynamicCast<RegularWifiMac> (wifi_mac);
    PointerValue ptr;
    rmac->GetAttribute ("Txop", ptr);
    Ptr<Txop> txop = ptr.Get<Txop> ();

    std::cout << "\nNode number: " << n << std::endl;
    std::cout << "MAc address: " << tmpAddress << std::endl;
    std::cout << "CW min value: " << txop->GetMinCw() << std::endl;
    std::cout << "CW max value: " << txop->GetMaxCw() << std::endl;
  }
}


class SimulationHelper 
{
public:
	SimulationHelper ();
	
	static OnOffHelper CreateOnOffHelper(InetSocketAddress socketAddress, DataRate dataRate, int packetSize, uint8_t tid, Time start, Time stop);
	static void PopulateArpCache ();
};

SimulationHelper::SimulationHelper () 
{
}

//prepare CBR traffic source
OnOffHelper
SimulationHelper::CreateOnOffHelper(InetSocketAddress socketAddress, DataRate dataRate, int packetSize, uint8_t tid, Time start, Time stop) 
{
  socketAddress.SetTos (tid << 5); //(see: https://www.tucny.com/Home/dscp-tos and http://www.revolutionwifi.net/revolutionwifi/2010/08/wireless-qos-part-3-user-priorities.html)

  OnOffHelper onOffHelper  ("ns3::UdpSocketFactory", socketAddress);
  onOffHelper.SetAttribute ("OnTime",     StringValue   ("ns3::ConstantRandomVariable[Constant=100000]"));
  onOffHelper.SetAttribute ("OffTime",    StringValue   ("ns3::ConstantRandomVariable[Constant=0]") );
  onOffHelper.SetAttribute ("DataRate",   DataRateValue (dataRate) );
  onOffHelper.SetAttribute ("PacketSize", UintegerValue (packetSize) );
  // onOffHelper.SetAttribute ("Jitter",     DoubleValue (1.0)); //packets generation times modified by random value between -50% and +50% of constant time step between packets
  onOffHelper.SetAttribute ("MaxBytes",   UintegerValue (0));
  onOffHelper.SetAttribute ("StartTime",  TimeValue (start));
  onOffHelper.SetAttribute ("StopTime",   TimeValue (stop));

  return onOffHelper;
}

void
PhyTxTrace (std::string context, Ptr<const Packet> packet, WifiMode mode, enum WifiPreamble preamble, uint8_t n)
{
  // std::size_t endPos = context.find("/DeviceList/");
  // std::string str = context.substr (10, endPos-1);
  // uint16_t node = std::atoi (str.c_str ());

  // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY TX START\t" << packet->GetUid () << std::endl;

  Ptr<Packet> p = packet->Copy ();
  AmpduTag ampdutag;
  if (p->PeekPacketTag (ampdutag))
    {
      AmpduSubframeHeader hdr;
      p->RemoveHeader (hdr);
    }
  WifiMacHeader macHdr;
  p->RemoveHeader (macHdr);

  if(macHdr.IsAck()) 
  {
      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr1 ())
          break;
      }

      
      stationInfoVec[counter].L2m_lastACKFrameUID = packet->GetUid();
  }

  if (macHdr.IsQosData ()) //data packet?
    {

//if (!macHdr.IsRetry ())
  //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY TX START\t" << packet->GetUid () << "\t" << *packet << std::endl;

      // uint8_t bufSrc[6]; //bufDst[6];
      // //macHdr.GetAddr1 ().CopyTo (bufDst);
      // macHdr.GetAddr2 ().CopyTo (bufSrc);

      // uint8_t std = bufSrc[3];
      // uint8_t grp = bufSrc[4];
      // uint8_t src = bufSrc[5] - 1;
      uint8_t tid = macHdr.GetQosTid ();
      if (tid > 7) tid = 0;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr2 ())
          break;
      }

      //std::cout << Simulator::Now ().GetSeconds () << "\tnode: " << node << "\tUID: " << (int)tid << "\tPHY TX START" << std::endl;

      if (preamble == 5
          && stationInfoVec[counter].m_prevPreambleTxed != 5)
        {
	      if (Simulator::Now () > Seconds (calcStart)) 
            stationInfoVec[counter].ampdusTxed++;

        }

      if (macHdr.IsQosAmsdu ())
        {
          WifiMacTrailer fcs;
          p->RemoveTrailer (fcs);
          MsduAggregator::DeaggregatedMsdus packets = MsduAggregator::Deaggregate (p);

	      if (Simulator::Now () > Seconds (calcStart)) 
            {
              if (!macHdr.IsRetry ())
                {
                  // std::cout << "\n Counter 1 amsdu txed up" << std::endl;
                  // std::cout << "AMSDU packet add for node " << node << std::endl;
                  // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY TX START\t" << packet->GetUid () << "\t" << *packet << std::endl;
                  stationInfoVec[counter].msdusTxed += packets.size ();
                  stationInfoVec[counter].amsdusTxed++;
                  stationInfoVec[counter].L2msdusTxed++;
                  stationInfoVec[counter].L2amsdusTxed++;
                  stationInfoVec[counter].L2m_sentFrames.insert (std::pair<uint64_t, Time> (packet->GetUid (), Simulator::Now ()) );

                }
              else
                {
                  // std::cout << "\n Counter 1 amsdu retry up" << std::endl;
                  stationInfoVec[counter].msdusRetry += packets.size ();
                  stationInfoVec[counter].amsdusRetry++;
                  stationInfoVec[counter].L2msdusRetry += packets.size ();
                  stationInfoVec[counter].L2amsdusRetry++;
                }
            }
        }
      else
        {
	      if (Simulator::Now () > Seconds (calcStart)) 
            {
              if (!macHdr.IsRetry ()){
                  // std::cout << "MSDU packet add for node " << node << std::endl;
                  // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY TX START\t" << packet->GetUid () << "\t" << *packet << std::endl;
                  stationInfoVec[counter].msdusTxed++;
                  stationInfoVec[counter].L2msdusTxed++;
                  stationInfoVec[counter].L2m_sentFrames.insert (std::pair<uint64_t, Time> (packet->GetUid (), Simulator::Now ()) );

                  // std::cout << "\n Counter 1 txed up" << std::endl;
                }
              else {
                // std::cout << "\n Counter 1 retry up" << std::endl;
                stationInfoVec[counter].msdusRetry++;
                stationInfoVec[counter].L2msdusRetry++;
              }
            }

        }

      stationInfoVec[counter].m_prevPreambleTxed = preamble;
      //m_lastFrameUID = packet->GetUid ();
    }
}

void 
PhyRxErrorTrace (std::string context, Ptr<const Packet> packet, double snr)
{
  // std::size_t endPos = context.find("/DeviceList/");
  // std::string str = context.substr (10, endPos-1);
  // uint16_t node = std::atoi (str.c_str ());

  // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY RX ERROR\t" << packet->GetUid () << ", SNR=" << snr << std::endl;

  if (packet->GetUid () != m_lastFrameUID) //to not process already processed collisions
    {
      std::size_t endPos = context.find("/DeviceList/");
      std::string str = context.substr (10, endPos-1);
      uint8_t node = std::atoi (str.c_str ());

      Ptr<Packet> p = packet->Copy ();

      AmpduTag ampdutag;
      if (p->PeekPacketTag (ampdutag))
        {
          AmpduSubframeHeader hdr;
          p->RemoveHeader (hdr);
        }
      WifiMacHeader macHdr;
      p->RemoveHeader (macHdr);

      //if (macHdr.IsData () || macHdr.IsQosData ()) //data packet?
      if (macHdr.IsQosData ()) //data packet?
        {
          // uint8_t bufSrc[6]; //bufDst[6];
          // //macHdr.GetAddr1 ().CopyTo (bufDst);
          // macHdr.GetAddr2 ().CopyTo (bufSrc);

          // uint8_t std = bufSrc[3];
          // uint8_t grp = bufSrc[4];
          // uint8_t src = bufSrc[5] - 1;
          uint8_t tid = macHdr.GetQosTid ();
          if (tid > 7) tid = 0;

          uint32_t counter = 0;
          for(;counter < stationInfoVec.size(); counter++){
            if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr2 ())
              break;
          }

          Ptr<NetDevice> dev = NodeList::GetNode (node)->GetDevice (0);

          if (Mac48Address::ConvertFrom (dev->GetAddress ()) == macHdr.GetAddr1 ())
            {
              if (Simulator::Now () > Seconds (calcStart)) 
                  stationInfoVec[counter].rxErrors++;

              //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
              // if(!macHdr.IsRetry()){
              //   Packets::iterator it = stationInfoVec[counter].L2m_sentFrames.find (packet->GetUid());
              //   stationInfoVec[counter].L2m_sentFrames.erase (it);
              // }
            }
        }
      else if (macHdr.IsAck () || macHdr.IsBlockAck () || macHdr.IsBlockAckReq ()) //ACK packet
        {
          // uint8_t bufDst[6];
          // macHdr.GetAddr1 ().CopyTo (bufDst);
          // uint8_t std = bufDst[3];
          // uint8_t grp = bufDst[4];
          // uint8_t dst = bufDst[5];

          uint32_t counter = 0;
          for(;counter < stationInfoVec.size(); counter++){
            if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr1 ())
              break;
          }

          Ptr<NetDevice> dev = NodeList::GetNode (node)->GetDevice (0);

          if (macHdr.GetAddr1 () == Mac48Address::ConvertFrom (dev->GetAddress ()) )
            {
              // uint8_t tid = stationInfoVec[counter].m_lastFrameTID;
	          if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].ackRxErrors++;

              m_lastFrameUID = packet->GetUid ();
            }            
        }
    }

}

void
PhyRxOkTrace (std::string context, Ptr<const Packet> packet, double snr, WifiMode mode, enum WifiPreamble preamble)
{
  std::size_t endPos = context.find("/DeviceList/");
  std::string str = context.substr (10, endPos-1);
  uint16_t node = std::atoi (str.c_str ());

  // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY RX OK\t" << packet->GetUid () << ", SNR=" << snr << std::endl;

  Ptr<Packet> p = packet->Copy ();
  AmpduTag ampdutag;
  if (p->PeekPacketTag (ampdutag))
    {
      AmpduSubframeHeader hdr;
      p->RemoveHeader (hdr);
    }
  WifiMacHeader macHdr;
  p->RemoveHeader (macHdr);

  if(macHdr.IsAck())
  {
      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr1 ())
          break;
      }

      if(stationInfoVec[counter].L2m_lastACKFrameUID == packet->GetUid())
      {
          Packets::iterator it = stationInfoVec[counter].L2m_sentFrames.find (stationInfoVec[counter].L2m_lastDATAFrameUID);

          if (it != stationInfoVec[counter].L2m_sentFrames.end ()) 
            {
              Time L2delay;
              L2delay = Simulator::Now () - it->second;
              if (Simulator::Now () > Seconds (calcStart))
                {
                  stationInfoVec[counter].L2msdusRxed++;
                  stationInfoVec[counter].L2delaySum  += L2delay;
                  stationInfoVec[counter].L2jitterSum += ( (stationInfoVec[counter].L2m_lastDelay > L2delay) 
                                                              ? (stationInfoVec[counter].L2m_lastDelay - L2delay) 
                                                              : (L2delay - stationInfoVec[counter].L2m_lastDelay) );
                }

              stationInfoVec[counter].L2m_lastDelay = L2delay;

              //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
              stationInfoVec[counter].L2m_sentFrames.erase (it);
            }
      }
  }

  if (macHdr.IsQosData ()) //data packet?
    {
      // std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tPHY DATA OK\t" << packet->GetUid () << ", SNR=" << snr << std::endl;

      // uint8_t bufSrc[6]; //bufDst[6];
      // //macHdr.GetAddr1 ().CopyTo (bufDst);
      // macHdr.GetAddr2 ().CopyTo (bufSrc);

      // uint8_t std = bufSrc[3];
      // uint8_t grp = bufSrc[4];
      // uint8_t src = bufSrc[5] - 1;
      uint8_t tid = macHdr.GetQosTid ();
      if (tid > 7) tid = 0;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == macHdr.GetAddr2 ())
          break;
      }



      Ptr<NetDevice> dev = NodeList::GetNode (node)->GetDevice (0);

      if (Mac48Address::ConvertFrom (dev->GetAddress ()) == macHdr.GetAddr1 ())
        {
          if (preamble == 5
              && stationInfoVec[counter].m_prevPreambleRxed != 5)
            {
	          if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].ampdusRxed++;
            }

          if (macHdr.IsQosAmsdu ())
            {
	          if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].amsdusRxed++;
            }
          
          if(!macHdr.IsRetry ()){
            stationInfoVec[counter].L2m_lastDATAFrameUID = packet->GetUid ();
            stationInfoVec[counter].m_prevPreambleRxed = preamble;
          }
          //m_lastFrameUID = packet->GetUid ();
        }
    }
}

//------------- MAC tracing --------------

void
MacTxTrace (std::string context, Ptr<const Packet> packet)
{
  //std::size_t endPos = context.find("/DeviceList/");
  //std::string str = context.substr (10, endPos-1);
  //uint16_t node = std::atoi (str.c_str ());

  //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tMAC TX\t" << packet->GetUid () << "\t" << *packet << std::endl;

  LlcSnapHeader llcHdr;
  Ptr<Packet> p = packet->Copy ();
  p->RemoveHeader (llcHdr);
  if (llcHdr.GetType () == 0x0800) //IP packet?
	{
	  Ipv4Header ipv4Hdr;
      p->RemoveHeader (ipv4Hdr);
	  // uint8_t std = (ipv4Hdr.GetSource ().Get () & 0x00ff0000) >> 16;
	  // uint8_t grp = (ipv4Hdr.GetSource ().Get () & 0x0000ff00) >> 8;
	  // uint8_t src = (ipv4Hdr.GetSource ().Get () & 0x000000ff) - 1;
	  uint8_t tid = QosUtilsGetTidForPacket (packet);
    if (tid > 7) tid = 0;

    uint32_t counter = 0;
    for(;counter < stationInfoVec.size(); counter++){
      if(stationInfoVec[counter].staAddress == ipv4Hdr.GetSource ())
        break;
    }

      //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tMAC TX\t" << packet->GetUid () << "\t" << *packet << std::endl;

	  if (Simulator::Now () > Seconds (calcStart)) 
        stationInfoVec[counter].msdusOrig++;

      //std::cout << "INSERT: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
    stationInfoVec[counter].m_sentFrames.insert (std::pair<uint64_t, Time> (packet->GetUid (), Simulator::Now ()) );

	}
}

void
MacRxOkTrace (std::string context, Ptr<const Packet> packet)
{
  //std::size_t endPos = context.find("/DeviceList/");
  //std::string str = context.substr (10, endPos-1);
  //uint16_t node = std::atoi (str.c_str ());

  //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node <<  "\tMAC RX OK\t" << packet->GetUid () << std::endl;
}

void 
MacTxFailTrace (std::string context, const WifiMacHeader &hdr, Ptr<const Packet> packet)
{
  //std::size_t endPos = context.find("/DeviceList/");
  //std::string str = context.substr (10, endPos-1);
  //uint16_t node = std::atoi (str.c_str ());

  //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node <<  "\tTRANSMISSION FAIL, FRAME DROP\t" << packet->GetUid () << "\tUP: " << int(QosUtilsGetTidForPacket (packet)) << std::endl;

  if (hdr.IsQosData ())
    {
      // uint8_t bufSrc[6]; //bufDst[6];
      // //hdr.GetAddr1 ().CopyTo (bufDst);
      // hdr.GetAddr2 ().CopyTo (bufSrc);

      // uint8_t std = bufSrc[3];
      // uint8_t grp = bufSrc[4];
      // uint8_t src = bufSrc[5] - 1;
      uint8_t tid = hdr.GetQosTid ();
      if (tid > 7) tid = 0;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == hdr.GetAddr2 ())
          break;
      }

      if (hdr.IsQosAmsdu ())
        {
          Ptr<Packet> p = packet->Copy ();
          WifiMacTrailer fcs;
          p->RemoveTrailer (fcs);
          MsduAggregator::DeaggregatedMsdus packets = MsduAggregator::Deaggregate (p);

          Packets::iterator it_1 = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it_1 != stationInfoVec[counter].m_sentFrames.end ()) 
            {
	          if (Simulator::Now () > Seconds (calcStart)) 
                {
                  stationInfoVec[counter].msduTxFails += packets.size ();
                  stationInfoVec[counter].amsduTxFails++;
                }


              Packets::iterator it_2;
              for (uint32_t i = 0; i < packets.size (); i++, it_2++) {/*std::cout << "ERASE: " << it_2->first << std::endl;*/};
              stationInfoVec[counter].m_sentFrames.erase (it_1, it_2);
            }
        }
      else
        {
          Packets::iterator it = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it != stationInfoVec[counter].m_sentFrames.end ()) 
            {
              if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].msduTxFails++;

              //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
              stationInfoVec[counter].m_sentFrames.erase (it);
            }
        }
    }
}

void
OverloadDropTrace (std::string context, const WifiMacHeader &hdr, Ptr<const Packet> packet)
{
  //std::size_t endPos = context.find("/DeviceList/");
  //std::string str = context.substr (10, endPos-1);
  //uint8_t src = std::atoi (str.c_str ());

  //std::cout << "QUEUE OVERLOAD DROP\t"  << ns3::Simulator::Now() << "\tUP: " << int(QosUtilsGetTidForPacket (packet)) << std::endl;

  if (hdr.IsQosData ())
    {
      // uint8_t bufSrc[6]; //bufDst[6];
      // //hdr.GetAddr1 ().CopyTo (bufDst);
      // hdr.GetAddr2 ().CopyTo (bufSrc);

      // uint8_t std = bufSrc[3];
      // uint8_t grp = bufSrc[4];
      // uint8_t src = bufSrc[5] - 1;
      uint8_t tid = hdr.GetQosTid ();
      if (tid > 7) tid = 0;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == hdr.GetAddr2 ())
          break;
      }

      if (hdr.IsQosAmsdu ())
        {
          Ptr<Packet> p = packet->Copy ();
          WifiMacTrailer fcs;
          p->RemoveTrailer (fcs);
          MsduAggregator::DeaggregatedMsdus packets = MsduAggregator::Deaggregate (p);

          Packets::iterator it_1 = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it_1 != stationInfoVec[counter].m_sentFrames.end ()) 
            {
              if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].overloadDrops += packets.size ();


              Packets::iterator it_2;
              for (uint32_t i = 0; i < packets.size (); i++, it_2++);
              stationInfoVec[counter].m_sentFrames.erase (it_1, it_2);
            }
        }
      else
        {
          Packets::iterator it = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it != stationInfoVec[counter].m_sentFrames.end ())
            {
              if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].overloadDrops++;

 
              //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
              stationInfoVec[counter].m_sentFrames.erase (it);
            }
        }
    }
}

void
LifetimeDropTrace (std::string context, const WifiMacHeader &hdr, Ptr<const Packet> packet)
{
  //std::size_t endPos = context.find("/DeviceList/");
  //std::string str = context.substr (10, endPos-1);
  //uint16_t node = std::atoi (str.c_str ());

  //std::cout << Simulator::Now ().GetMicroSeconds () << "\tnode: " << node << "\tFRAME LIFETIME DROP\t" << packet->GetUid () << "\tUP: " << int(QosUtilsGetTidForPacket (packet)) << std::endl;

  if (hdr.IsQosData ())
    {
      // uint8_t bufSrc[6]; //bufDst[6];
      // //hdr.GetAddr1 ().CopyTo (bufDst);
      // hdr.GetAddr2 ().CopyTo (bufSrc);

      // uint8_t std = bufSrc[3];
      // uint8_t grp = bufSrc[4];
      // uint8_t src = bufSrc[5] - 1;
      uint8_t tid = hdr.GetQosTid ();
      if (tid > 7) tid = 0;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staMacAddress == hdr.GetAddr2 ())
          break;
      }

      if (hdr.IsQosAmsdu ())
        {
          Ptr<Packet> p = packet->Copy ();
          WifiMacTrailer fcs;
          p->RemoveTrailer (fcs);
          MsduAggregator::DeaggregatedMsdus packets = MsduAggregator::Deaggregate (p);

          Packets::iterator it_1 = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it_1 != stationInfoVec[counter].m_sentFrames.end ()) 
            {
	          if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].lifetimeDrops += packets.size ();


              Packets::iterator it_2;
              for (uint32_t i = 0; i < packets.size (); i++, it_2++);
              stationInfoVec[counter].m_sentFrames.erase (it_1, it_2);
            }
        }
      else
        {
          Packets::iterator it = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

          if (it != stationInfoVec[counter].m_sentFrames.end ()) 
            {
              if (Simulator::Now () > Seconds (calcStart)) 
                stationInfoVec[counter].lifetimeDrops++;


              //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
              stationInfoVec[counter].m_sentFrames.erase (it);
            }
        }
    }
}

//------------- IPv4 tracing -------------


void
SendOutgoing (std::string context, const Ipv4Header &hdr, Ptr<const Packet> packet, uint32_t iif)
{
  //std::cout << Simulator::Now ().GetSeconds () <<  "\tIpv4L3 TX OK " << " " << packet->GetUid () << std::endl;
}

void
RxOkTrace (std::string context, const Ipv4Header &hdr, Ptr<const Packet> packet, uint32_t iif)
{
  //std::cout << Simulator::Now ().GetSeconds () <<  "\tIpv4L3 RX OK " << " " << packet->GetUid () << std::endl;

  // uint8_t std = (hdr.GetSource ().Get () & 0x00ff0000) >> 16;
  // uint8_t grp = (hdr.GetSource ().Get () & 0x0000ff00) >> 8;
  // uint8_t src = (hdr.GetSource ().Get () & 0x000000ff) - 1;
  uint8_t tid = hdr.GetTos () >> 5;
  uint32_t counter = 0;
  for(;counter < stationInfoVec.size(); counter++){
    if(stationInfoVec[counter].staAddress == hdr.GetSource ())
      break;
  }

  Packets::iterator it = stationInfoVec[counter].m_sentFrames.find (packet->GetUid ());

  Time delay;
  delay = Simulator::Now () - it->second;

  if (Simulator::Now () > Seconds (calcStart))
    {
	  stationInfoVec[counter].msdusRxed++;
	  stationInfoVec[counter].recvBytes += (packet->GetSize () + 20 + 8); //with Ipv4 and LLC headers
	  stationInfoVec[counter].delaySum  += delay;
      stationInfoVec[counter].jitterSum += ( (stationInfoVec[counter].m_lastDelay > delay) 
                                                ? (stationInfoVec[counter].m_lastDelay - delay) 
                                                : (delay - stationInfoVec[counter].m_lastDelay) );
    }

  stationInfoVec[counter].m_lastDelay = delay;
  stationInfoVec[counter].m_lastFrameTID = tid;
  //std::cout << "ERASE: " << packet->GetUid () << ", std: " << (int)std << ", grp: " << (int)grp << ", src: " << (int)src << ", tid: " << (int)tid << std::endl;
  stationInfoVec[counter].m_sentFrames.erase (it);
}




//fullfil the ARP cache prior to simulation run
void
SimulationHelper::PopulateArpCache () 
{
  Ptr<ArpCache> arp = CreateObject<ArpCache> ();
  arp->SetAliveTimeout (Seconds (3600 * 24 * 365) );
	
  for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i) 
    {	
      Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
      NS_ASSERT (ip != 0);
      ObjectVectorValue interfaces;
      ip->GetAttribute ("InterfaceList", interfaces);

      for (ObjectVectorValue::Iterator j = interfaces.Begin (); j != interfaces.End (); j++) 
        {		
          Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
          NS_ASSERT (ipIface != 0);
          Ptr<NetDevice> device = ipIface->GetDevice ();
          NS_ASSERT (device != 0);
          Mac48Address addr = Mac48Address::ConvertFrom (device->GetAddress () );
      
          for (uint32_t k = 0; k < ipIface->GetNAddresses (); k++) 
            {			
              Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal();		
              if (ipAddr == Ipv4Address::GetLoopback ()) 
                continue;

              ArpCache::Entry *entry = arp->Add (ipAddr);
              Ipv4Header ipv4Hdr;
              ipv4Hdr.SetDestination (ipAddr);
              Ptr<Packet> p = Create<Packet> (100);  
              entry->MarkWaitReply (ArpCache::Ipv4PayloadHeaderPair (p, ipv4Hdr) );
              entry->MarkAlive (addr);
            }
        }
    }

    for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i) 
      {
        Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
		NS_ASSERT (ip != 0);
		ObjectVectorValue interfaces;
		ip->GetAttribute ("InterfaceList", interfaces);

        for (ObjectVectorValue::Iterator j = interfaces.Begin (); j != interfaces.End (); j ++)
          {
            Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
            ipIface->SetAttribute ("ArpCache", PointerValue (arp) );
          }
      }
}





/* ===== main function ===== */

int main (int argc, char *argv[])
{
  uint32_t nSTA54 = 1;
  uint32_t packetSize = 1470;
  float radius = 1.0;
  bool oneDest = true;
  bool rtsCts = false;
  bool VO = true;
  bool VI = true;
  bool BE = true;
  bool BK = true;
  double Mbps = 10;    //z takim datarate wysylam
  double channelWidth = 20;
  bool shortGuard = false;
  uint32_t nbAntennas = 1;
  uint32_t seed = 0;
  std::string dataMode = "HtMcs7";
  std::string controlMode="HtMcs0";


/* ===== Command Line parameters ===== */

  CommandLine cmd;
  //cmd.AddValue ("nSTA",       "Number of stations",                            nSTA);
  cmd.AddValue ("nSTA",       "Number of stations",                            nSTA54);
  cmd.AddValue ("packetSize", "Packet size [B]",                               packetSize);
  cmd.AddValue ("simTime",    "simulation time [s]",                           simTime);
  cmd.AddValue ("calcStart",  "start of results analysis [s]",                 calcStart);
  cmd.AddValue ("radius",     "Radius of area [m] to randomly place stations", radius);
  cmd.AddValue ("oneDest",    "use one traffic destination?",                  oneDest);
  cmd.AddValue ("RTSCTS",     "use RTS/CTS?",                                  rtsCts);
  cmd.AddValue ("VO",         "run VO traffic?",                               VO);
  cmd.AddValue ("VI",         "run VI traffic?",                               VI);
  cmd.AddValue ("BE",         "run BE traffic?",                               BE);
  cmd.AddValue ("BK",         "run BK traffic?",                               BK);
  cmd.AddValue ("Mbps",       "traffic generated per queue [Mbps]",            Mbps);
  cmd.AddValue ("seed",       "seed value that determines simulation. Default 0-> random seed",         seed);
  cmd.AddValue ("anomCWmin",   "initial CW min value for anomaly station",      anomCWmin);
  cmd.AddValue ("channelWidth",       "Channel Width for 802.11n",            channelWidth);
  cmd.AddValue ("shortGuard",       "Enable/disable shortGuard",            shortGuard);
  cmd.AddValue ("nbAntennas",       "Set number of antennas",            nbAntennas);
  cmd.Parse (argc, argv);

  std::string directory = "ENGINEERING_THESIS/STANDARD_80211ac_RESULTS";
  std::string filepath = "load_" + std::to_string((int)Mbps) 
                          + "_stations_" + std::to_string(nSTA54) 
                          + "_seed_" + std::to_string(seed) 
                          + "_channelwidth_"+std::to_string((int)channelWidth)
                          + "_shortguard_"+std::to_string(shortGuard)
                          + "_antennas_"+std::to_string(nbAntennas)
                          +".csv";

  // Time appsStart = Seconds(calcStart);
  Time appsStart = Seconds(calcStart-1);

  // switch(nbAntennas){
  //   case 1:
  //     dataMode = "HtMcs7";
  //     controlMode = "HtMcs0";
  //     break;
  //   case 2:
  //     dataMode = "HtMcs15";
  //     controlMode = "HtMcs8";
  //     break;
  //   case 3:
  //     dataMode = "HtMcs23";
  //     controlMode = "HtMcs16";
  // }

  stationInfoVec = std::vector<StationInformation>(nSTA54+nSTA*2, StationInformation());

  Time simulationTime = Seconds (simTime);
  
  std::cout << "Seed manager" << std::endl;
  std::cout << "Seed value: " << seed << std::endl;
  if(seed == 0){
    std::cout << "Adding random seed" << std::endl;
    time_t timev;
    time(&timev);
    RngSeedManager::SetSeed(timev);
  } else {
    RngSeedManager::SetSeed(seed);
  }

  // std::cout << "DataMode: " << dataMode << std::endl;
  // std::cout << "ControlMode: " << controlMode << std::endl;

 
  Packet::EnablePrinting ();

  
  // Access Point number
  NodeContainer AP;
  AP.Create (nSTA);

  // Anomaly station number
  NodeContainer sta2;
  sta2.Create (nSTA);
  
  // High-Speed station number
  NodeContainer sta3;
  sta3.Create (nSTA54);

  // std::cout << "Nodes to create: " << nSTA54+nSTA*2 << std::endl;

  // std::cout << "Create AP nodes: " << nSTA << std::endl;
  for(uint32_t n = 0; n < nSTA; n++) {
    // std::cout << "\tAP node number: " << n << std::endl;
    stationInfoVec[n].staNode = AP.Get(n);
  }

  // std::cout << "Create Low-Speed nodes: " << nSTA54 << std::endl;
  for(uint32_t n = 0; n < nSTA; n++) {
    // std::cout << "\tLow-Speed node number: " << n+nSTA << std::endl;
    stationInfoVec[n+nSTA].staNode = sta2.Get(n);
  }

  // std::cout << "Create High-Speed nodes: "<< nSTA << std::endl;
  for(uint32_t n = 0; n < nSTA54; n++) {
    // std::cout << "\tHigh-Speed node number: " << n+nSTA+nSTA54 << std::endl;
    stationInfoVec[n+nSTA+nSTA].staNode = sta3.Get(n);
  }



  // std::cout << "Set up mobility position" << std::endl;

/* ======== Positioning / Mobility ======= */
  
  //UniformDiscPositionAllocator - uniform distiburion of nodes on disc area
  Ptr<UniformDiscPositionAllocator> positionAlloc = CreateObject<UniformDiscPositionAllocator> ();
  positionAlloc->SetX   (0.0); positionAlloc->SetY   (0.0); //set disc center
  positionAlloc->SetRho (radius); //area radius

  //ListPositionAllocator used for uniform distiburion of nodes on the circle around central node
  /*Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0)); //1st node/AP located in the center
  for (uint32_t i = 0; i < nSTA; i++)
    positionAlloc->Add (Vector (radius * sin (2*M_PI * (float)i/(float)nSTA), radius * cos (2*M_PI * (float)i/(float)nSTA), 0.0));*/

  MobilityHelper mobility1;
  mobility1.SetPositionAllocator (positionAlloc);
  mobility1.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); //kreca sie ze stala predkoscia -- jak chce zeby w miejscu to ConstantPositionMobilityModel

  //constant speed movement configuration
  /*Ptr<ConstantVelocityMobilityModel> mob = sta[std][grp].Get (n)->GetObject<ConstantVelocityMobilityModel> ();
  mob->SetVelocity (Vector3D (movX, movY, movZ) );*/

  mobility1.Install (AP);
  mobility1.Install (sta2);
  mobility1.Install (sta3);  
  

/* ===== Propagation Model configuration ===== */

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();



/* ===== MAC and PHY configuration ===== */

  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifiAP;
  WifiMacHelper macAP;
  wifiAP.SetStandard (WIFI_PHY_STANDARD_80211ac);
  WifiHelper wifiSta2;
  WifiMacHelper macSta2;
  wifiSta2.SetStandard (WIFI_PHY_STANDARD_80211ac);
  WifiHelper wifiSta3;
  WifiMacHelper macSta3;
  wifiSta3.SetStandard (WIFI_PHY_STANDARD_80211ac);
  //QosWifiMacHelper mac = HtWifiMacHelper::Default (); //802.11n
  //wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
  //QosWifiMacHelper mac = VhtWifiMacHelper::Default (); //802.11ac
  //wifi.SetStandard (WIFI_PHY_STANDARD_80211ac);


  //PHY parameters 
  //for complete list of available parameters - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_wifi_phy.html#pri-static-attribs
  phy.Set ("RxNoiseFigure",                DoubleValue   (7.0) );
  phy.Set ("TxPowerStart",                 DoubleValue   (15.0) );
  phy.Set ("TxPowerEnd",                   DoubleValue   (15.0) );
  phy.Set ("CcaMode1Threshold",            DoubleValue   (-82.0) );
  phy.Set ("EnergyDetectionThreshold",     DoubleValue   (-88.0) );
  phy.Set ("Antennas",                     UintegerValue (nbAntennas) ); //for 802.11n/ac - see http://mcsindex.com/     !!!!!
  phy.Set ("MaxSupportedTxSpatialStreams", UintegerValue (nbAntennas) ); //for 802.11n/ac - see http://mcsindex.com/
  phy.Set ("MaxSupportedRxSpatialStreams", UintegerValue (nbAntennas) ); //for 802.11n/ac - see http://mcsindex.com/
  phy.Set ("ShortGuardEnabled",            BooleanValue  (shortGuard) ); //for 802.11n/ac - see http://mcsindex.com/
  

  //WiFi Remote Station Manager parameters 

  //Constant Rate setting - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_constant_rate_wifi_manager.html#pri-attribs
  wifiAP.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
							    // "DataMode",    StringValue ("OfdmRate54Mbps"), //802.11a
							    // "ControlMode", StringValue ("OfdmRate6Mbps"),  //802.11a
							    // "DataMode",    StringValue (dataMode), //802.11n - see http://mcsindex.com/
							    // "ControlMode", StringValue (controlMode), //802.11n - see http://mcsindex.com/
							    "DataMode",    StringValue ("VhtMcs9"), //802.11ac - see http://mcsindex.com/
							    "ControlMode", StringValue ("VhtMcs0"), //802.11ac - see http://mcsindex.com/
								"MaxSsrc", UintegerValue (7), //changed - Short Retry Counter (SRC) limit!
								"MaxSlrc", UintegerValue (4), //changed - Long Retry Counter (LRC) limit!
							    "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
                  "FragmentationThreshold", UintegerValue (2500));
  wifiSta2.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
							    // "DataMode",    StringValue ("OfdmRate6Mbps"), //802.11a
							    // "ControlMode", StringValue ("OfdmRate6Mbps"),  //802.11a
							    // "DataMode",    StringValue (controlMode), //802.11n - see http://mcsindex.com/
							    // "ControlMode", StringValue (controlMode), //802.11n - see http://mcsindex.com/
							    "DataMode",    StringValue ("VhtMcs0"), //802.11ac - see http://mcsindex.com/
							    "ControlMode", StringValue ("VhtMcs0"), //802.11ac - see http://mcsindex.com/
								"MaxSsrc", UintegerValue (7), //changed - Short Retry Counter (SRC) limit!
								"MaxSlrc", UintegerValue (4), //changed - Long Retry Counter (LRC) limit!
							    "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
							    "FragmentationThreshold", UintegerValue (2500));
  wifiSta3.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
							    // "DataMode",    StringValue ("OfdmRate54Mbps"), //802.11a
							    // "ControlMode", StringValue ("OfdmRate6Mbps"),  //802.11a
							    // "DataMode",    StringValue (dataMode), //802.11n - see http://mcsindex.com/
							    // "ControlMode", StringValue (controlMode), //802.11n - see http://mcsindex.com/
							    "DataMode",    StringValue ("VhtMcs9"), //802.11ac - see http://mcsindex.com/
							    "ControlMode", StringValue ("VhtMcs0"), //802.11ac - see http://mcsindex.com/
								"MaxSsrc", UintegerValue (7), //changed - Short Retry Counter (SRC) limit!
								"MaxSlrc", UintegerValue (4), //changed - Long Retry Counter (LRC) limit!
							    "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
							    "FragmentationThreshold", UintegerValue (2500));

  //IDEAL rate manager for 802.11a - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_ideal_wifi_manager.html#pri-attribs
  /*wifi.SetRemoteStationManager ("ns3::IdealWifiManager", 
							      "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
							      "FragmentationThreshold", UintegerValue (2500));*/

  //MINSTREL rate manager for 802.11a - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_minstrel_wifi_manager.html#pri-attribs
  /*wifi.SetRemoteStationManager ("ns3::MinstrelWifiManager", 
							      "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
							      "FragmentationThreshold", UintegerValue (2500));*/

  //MINSTREL rate manager for 802.11n/ac - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_minstrel_ht_wifi_manager.html#pri-attribs
  /*wifi.SetRemoteStationManager ("ns3::MinstrelHtWifiManager", 
							      "RtsCtsThreshold",        UintegerValue (rtsCts ? 0 : 2500),
							      "FragmentationThreshold", UintegerValue (2500));*/

  //MAC parameters
  //for complete list of available parameters - see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_adhoc_wifi_mac.html#pri-methods
  //mac.SetType ("ns3::AdhocWifiMac",
  //             "Ssid", SsidValue (Ssid ("TEST")) );
  macAP.SetType("ns3::AdhocWifiMac",
                "Ssid", SsidValue (Ssid ("TEST")),
                // "BeaconGeneration", BooleanValue (false),
                "VO_MaxAmsduSize",    UintegerValue (0),
                "VI_MaxAmsduSize",    UintegerValue (0),
                "BE_MaxAmsduSize",    UintegerValue (0) ,
                "BK_MaxAmsduSize",    UintegerValue (0) ,
                "VO_MaxAmpduSize",    UintegerValue (0),
                "VI_MaxAmpduSize",    UintegerValue (0),
                "BE_MaxAmpduSize",    UintegerValue (0) ,
                "BK_MaxAmpduSize",    UintegerValue (0) ,
                "QosSupported", BooleanValue(true));
  macSta2.SetType("ns3::AdhocWifiMac",
                  "Ssid", SsidValue (Ssid ("TEST")),
                  // "ActiveProbing", BooleanValue (false),
                  "VO_MaxAmsduSize",    UintegerValue (0) ,
                  "VI_MaxAmsduSize",    UintegerValue (0) ,
                  "BE_MaxAmsduSize",    UintegerValue (0) ,
                  "BK_MaxAmsduSize",    UintegerValue (0) ,
                  "VO_MaxAmpduSize",    UintegerValue (0) ,
                  "VI_MaxAmpduSize",    UintegerValue (0) ,
                  "BE_MaxAmpduSize",    UintegerValue (0) ,
                  "BK_MaxAmpduSize",    UintegerValue (0) ,
                  "QosSupported", BooleanValue(true) );
  macSta3.SetType("ns3::AdhocWifiMac",
                  "Ssid", SsidValue (Ssid ("TEST")),
                  // "ActiveProbing", BooleanValue (false),
                  "VO_MaxAmsduSize",    UintegerValue (0) ,
                  "VI_MaxAmsduSize",    UintegerValue (0) ,
                  "BE_MaxAmsduSize",    UintegerValue (0) ,
                  "BK_MaxAmsduSize",    UintegerValue (0) ,
                  "VO_MaxAmpduSize",    UintegerValue (0) ,
                  "VI_MaxAmpduSize",    UintegerValue (0) ,
                  "BE_MaxAmpduSize",    UintegerValue (0) ,
                  "BK_MaxAmpduSize",    UintegerValue (0) ,
                  "QosSupported", BooleanValue(true));

  //example - MAC config with aggregation control for 802.11ac
  /*mac.SetType ("ns3::AdhocWifiMac",
               "VO_MaxAmsduSize",    UintegerValue (11398) ),
               "VI_MaxAmsduSize",    UintegerValue (11398) ),
               "BE_MaxAmsduSize",    UintegerValue (11398) ),
               "BK_MaxAmsduSize",    UintegerValue (11398) ),
               "VO_MaxAmpduSize",    UintegerValue (1048575) ),
               "VI_MaxAmpduSize",    UintegerValue (1048575) ),
               "BE_MaxAmpduSize",    UintegerValue (1048575) ),
               "BK_MaxAmpduSize",    UintegerValue (1048575) ),
               "Ssid", SsidValue (Ssid ("TEST")) );*/

  NetDeviceContainer staDevicesAP = wifiAP.Install (phy, macAP, AP);
  NetDeviceContainer staDevicesSta2 = wifiSta2.Install (phy, macSta2, sta2);
  NetDeviceContainer staDevicesSta3 = wifiSta3.Install (phy, macSta3, sta3);


//Configs with paths:  --szerokosc pasma XDDDDDDDDDDDDDDDDDDD
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (channelWidth) ); //for 802.11n/ac - see http://mcsindex.com/

//EDCA parameters:
//see Attributes on https://www.nsnam.org/doxygen/classns3_1_1_dca_txop.html#friends
  
//MinCw:    -- od tego mozna sobie uzaleznic ta swoja os pozioma 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_Txop/MinCw", UintegerValue (3) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_Txop/MinCw", UintegerValue (7) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_Txop/MinCw", UintegerValue (15) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BK_Txop/MinCw", UintegerValue (15) );

//MaxCw:    -- od tego mozna sobie uzaleznic ta swoja os pozioma
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MaxCw", UintegerValue (4096) );
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_Txop/MaxCw", UintegerValue (4096) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_Txop/MaxCw", UintegerValue (4096) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_Txop/MaxCw", UintegerValue (4096) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BK_Txop/MaxCw", UintegerValue (4096) );

//TXOP limit:   --to tak samo
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_Txop/TxopLimit", TimeValue (MicroSeconds (1504) ) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_Txop/TxopLimit", TimeValue (MicroSeconds (3008) ) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_Txop/TxopLimit", TimeValue (MicroSeconds    (0) ) ); 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BK_Txop/TxopLimit", TimeValue (MicroSeconds    (0) ) );

//EDCA max delay (NEW):
  //Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_Txop/MaxDelay", TimeValue (MicroSeconds  (10240) ) ); //setting VO packet lifetime = 10*TU (TU=1024 us)
  //Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_Txop/MaxDelay", TimeValue (MicroSeconds (102400) ) ); //setting VI packet lifetime = 100*TU (TU=1024 us)
  //Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_Txop/MaxDelay", TimeValue (MicroSeconds (512000) ) ); //setting BE packet lifetime = 500*TU (TU=1024 us)
  //Config::Set ("/NodeList/*/DeviceList/*/Mac/BK_Txop/MaxDelay", TimeValue (MicroSeconds (512000) ) ); //setting BK packet lifetime = 500*TU (TU=1024 us)

  Config::Set ("/NodeList/*/DeviceList/*/Mac/VO_Txop/Queue/MaxSize", QueueSizeValue(QueueSize("10000p"))); //setting VO queue size 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/VI_Txop/Queue/MaxSize", QueueSizeValue(QueueSize("10000p"))); //setting VI queue size 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BE_Txop/Queue/MaxSize", QueueSizeValue(QueueSize("10000p"))); //setting BE queue size 
  Config::Set ("/NodeList/*/DeviceList/*/Mac/BK_Txop/Queue/MaxSize", QueueSizeValue(QueueSize("10000p"))); //setting BK queue size 

  //int64_t streamIndex = 0;
  //wifi.AssignStreams (staDevices, streamIndex);


/* ===== Set up anomaly CW min ===== */

//MinCw anomaly:    -- od tego mozna sobie uzaleznic ta swoja os pozioma 
  std::cout << "Anomaly CW min value: " << anomCWmin << std::endl;
  Config::Set ("/NodeList/1/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MinCw", UintegerValue (anomCWmin) );
  Config::Set ("/NodeList/1/DeviceList/*/Mac/VO_Txop/MinCw", UintegerValue (anomCWmin) ); 
  Config::Set ("/NodeList/1/DeviceList/*/Mac/VI_Txop/MinCw", UintegerValue (anomCWmin) ); 
  Config::Set ("/NodeList/1/DeviceList/*/Mac/BE_Txop/MinCw", UintegerValue (anomCWmin) ); 
  Config::Set ("/NodeList/1/DeviceList/*/Mac/BK_Txop/MinCw", UintegerValue (anomCWmin) );


  GetCw();


/* ===== Internet stack ===== */

  InternetStackHelper stack;
  stack.Install (AP);
  stack.Install (sta2);
  stack.Install (sta3);

  Ipv4AddressHelper address;

  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer staIfAP;
  Ipv4InterfaceContainer staIfSta2;
  Ipv4InterfaceContainer staIfSta3;
  staIfAP = address.Assign (staDevicesAP);
  staIfSta2 = address.Assign (staDevicesSta2);
  staIfSta3 = address.Assign (staDevicesSta3);

  for(uint32_t n=0; n < nSTA;n++) {
    stationInfoVec[n].staAddress = staIfAP.GetAddress(n);
  }
  
  for(uint32_t n=0; n < nSTA;n++) {
    stationInfoVec[n+nSTA].staAddress = staIfSta2.GetAddress(n);
  }

  for(uint32_t n = 0; n < nSTA54; n++) {
    stationInfoVec[n+nSTA+nSTA].staAddress = staIfSta3.GetAddress(n);
  }

  for(uint32_t n=0; n < nSTA*2+nSTA54; n++) {
    Ptr<NetDevice> dev = NodeList::GetNode (n)->GetDevice (0);
    stationInfoVec[n].staMacAddress = Mac48Address::ConvertFrom (dev->GetAddress ());
  }


  // GetCw();




/* ===== Setting applications ===== */

  DataRate dataRate2 = DataRate ((1000000 * Mbps)/6);
  // DataRate dataRate2 = DataRate ((1000000 * Mbps)/nSTA);
  DataRate dataRate3 = DataRate ((1000000 * Mbps)/nSTA54);

  uint32_t destinationSTANumber = 0; //for one common traffic destination

  Ipv4Address destination = staIfAP.GetAddress(destinationSTANumber);
  Ptr<Node> dest = AP.Get(destinationSTANumber);
//   Ipv4Address destination3 = staIfSta3.GetAddress(destinationSTANumber);
//   Ptr<Node> dest3 = sta3.Get(destinationSTANumber);


  if (oneDest)
    {
      if (VO) 
        {
          PacketSinkHelper sink_VO ("ns3::UdpSocketFactory", InetSocketAddress (destination, 1006));
          sink_VO.Install (dest);               //punkt do ktorego zlewa wszystkie dane ....sink....
        }
      if (VI) 
        {
          PacketSinkHelper sink_VI ("ns3::UdpSocketFactory", InetSocketAddress (destination, 1005));
          sink_VI.Install (dest);
        }
      if (BE) 
        {
          PacketSinkHelper sink_BE ("ns3::UdpSocketFactory", InetSocketAddress (destination, 1000));
          sink_BE.Install (dest);
        }
      if (BK) 
        {
          PacketSinkHelper sink_BK ("ns3::UdpSocketFactory", InetSocketAddress (destination, 1001));
          sink_BK.Install (dest);
        }
    }

    Ptr<Node> nodeSta2 = sta2.Get(0);

    if (VO) 
    {
      OnOffHelper onOffHelper_VOSta2 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1006), dataRate2, packetSize, 6, appsStart, simulationTime);
      onOffHelper_VOSta2.Install(nodeSta2);
    }
  if (VI) 
    {
      OnOffHelper onOffHelper_VISta2 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1005), dataRate2, packetSize, 5, appsStart, simulationTime);
      onOffHelper_VISta2.Install(nodeSta2);
      }
  if (BE) 
    {
      OnOffHelper onOffHelper_BESta2 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1000), dataRate2, packetSize, 0, appsStart, simulationTime);
      onOffHelper_BESta2.Install(nodeSta2);
      }
  if (BK) 
    {
      OnOffHelper onOffHelper_BKSta2 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1001), dataRate2, packetSize, 1, appsStart, simulationTime);
      onOffHelper_BKSta2.Install(nodeSta2);
      }

  for(uint32_t i = 0; i < nSTA54; i++) 
    {
      
      Ptr<Node> nodeSta3 = sta3.Get(i);

    //   if (!oneDest) //overwrite for different traffic destinations
    //     {
    //       destinationSTANumber = 0; 
    //       destination2 = staIfSta2.GetAddress(destinationSTANumber);
    //       dest2 = sta2.Get(destinationSTANumber);
    //       destinationSTANumber = 0; 
    //       destination3 = staIfSta3.GetAddress(destinationSTANumber);
    //       dest3 = sta3.Get(destinationSTANumber);
    //     }

       if (VO) 
         {
           OnOffHelper onOffHelper_VOSta3 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1006), dataRate3, packetSize, 6, appsStart, simulationTime);
           onOffHelper_VOSta3.Install(nodeSta3);
         }
       if (VI) 
         {
           OnOffHelper onOffHelper_VISta3 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1005), dataRate3, packetSize, 5, appsStart, simulationTime);
           onOffHelper_VISta3.Install(nodeSta3);
         }
       if (BE) 
         {
           OnOffHelper onOffHelper_BESta3 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1000), dataRate3, packetSize, 0, appsStart, simulationTime);
           onOffHelper_BESta3.Install(nodeSta3);
         }
       if (BK) 
         {
           OnOffHelper onOffHelper_BKSta3 = SimulationHelper::CreateOnOffHelper(InetSocketAddress (destination, 1001), dataRate3, packetSize, 1, appsStart, simulationTime);
           onOffHelper_BKSta3.Install(nodeSta3);
      }
    }


  
  
  Config::Connect ("/NodeList/*/DeviceList/*/Phy/State/Tx",      MakeCallback (&PhyTxTrace) );
  Config::Connect ("/NodeList/*/DeviceList/*/Phy/State/RxError", MakeCallback (&PhyRxErrorTrace) );
  Config::Connect ("/NodeList/*/DeviceList/*/Phy/State/RxOk",    MakeCallback (&PhyRxOkTrace) );

  Config::Connect ("/NodeList/*/DeviceList/*/Mac/MacTx",         MakeCallback (&MacTxTrace) );
  Config::Connect ("/NodeList/*/DeviceList/*/Mac/MacRx",         MakeCallback (&MacRxOkTrace) );

    //IPv4 tracing
  Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/LocalDeliver", MakeCallback (&RxOkTrace) );
  Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/SendOutgoing", MakeCallback (&SendOutgoing) );




/* ===== tracing configuration and running simulation === */

  SimulationHelper::PopulateArpCache ();
  //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  Simulator::Stop (simulationTime);

  //phy.EnablePcap ("out", nSTA-1, 0); // sniffing to pcap file
  //AsciiTraceHelper ascii;
  //phy.EnableAsciiAll (ascii.CreateFileStream ("out.tr"));
  //phy.EnableAscii (ascii.CreateFileStream ("out.tr"), sta.Get (1)->GetDevice (1));
  //mac.EnableAsciiAll (ascii.CreateFileStream ("out.tr"));

  FlowMonitorHelper flowmon_helper;
  Ptr<FlowMonitor> monitor = flowmon_helper.InstallAll ();
  monitor->SetAttribute ("StartTime", TimeValue (Seconds (calcStart) ) ); //Time from which flowmonitor statistics are gathered.
  monitor->SetAttribute ("DelayBinWidth", DoubleValue (0.001));
  monitor->SetAttribute ("JitterBinWidth", DoubleValue (0.001));
  monitor->SetAttribute ("PacketSizeBinWidth", DoubleValue (20));

  Simulator::Run ();

  // GetCw();

  Simulator::Destroy ();



/* ===== printing results ===== */

  monitor->CheckForLostPackets();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon_helper.GetClassifier ());
  //monitor->SerializeToXmlFile ("out.xml", true, true);

  std::string proto;
  uint64_t txBytes = 0, rxBytes = 0, txPackets = 0, rxPackets = 0, lostPackets = 0;
  double throughput;
  Time delaySum = Seconds (0), jitterSum = Seconds (0);

  std::vector<uint64_t> txBytesPerTid     = std::vector<uint64_t> (8, 0);
  std::vector<uint64_t> rxBytesPerTid     = std::vector<uint64_t> (8, 0);
  std::vector<uint64_t> txPacketsPerTid   = std::vector<uint64_t> (8, 0);
  std::vector<uint64_t> rxPacketsPerTid   = std::vector<uint64_t> (8, 0);
  std::vector<uint64_t> lostPacketsPerTid = std::vector<uint64_t> (8, 0);
  std::vector<double>   throughputPerTid  = std::vector<double>   (8, 0.0);
  std::vector<Time>     delaySumPerTid    = std::vector<Time>     (8, Seconds (0) );
  std::vector<Time>     jitterSumPerTid   = std::vector<Time>     (8, Seconds (0) );

  std::map< FlowId, FlowMonitor::FlowStats > stats = monitor->GetFlowStats();
  for (std::map< FlowId, FlowMonitor::FlowStats >::iterator flow = stats.begin (); flow != stats.end (); flow++)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (flow->first);
      switch (t.protocol)
        {
          case (6):
            proto = "TCP";              //po luj to
            break;
          case (17):
            proto = "UDP";
            break;
          default:
            exit (1);
        }
      std::cout << "FlowID: " << flow->first << "(" << proto << " "
                << t.sourceAddress << "/" << t.sourcePort << " --> "
                << t.destinationAddress << "/" << t.destinationPort << ")" <<
      std::endl;

      std::cout << "  Tx bytes:\t"     << flow->second.txBytes << std::endl;
      std::cout << "  Rx bytes:\t"     << flow->second.rxBytes << std::endl;
      std::cout << "  Tx packets:\t"   << flow->second.txPackets << std::endl;
      std::cout << "  Rx packets:\t"   << flow->second.rxPackets << std::endl;
      std::cout << "  Lost packets:\t" << flow->second.lostPackets << std::endl;

      uint32_t counter = 0;
      for(;counter < stationInfoVec.size(); counter++){
        if(stationInfoVec[counter].staAddress == t.sourceAddress)
          break;
      }

      if (flow->second.rxPackets > 0)
        {
          stationInfoVec[counter].Throughput = flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds ();
          stationInfoVec[counter].Mean_Delay = (double)(flow->second.delaySum / (flow->second.rxPackets)).GetMicroSeconds () / 1000;
          //std::cout << "  Throughput:\t"   << flow->second.rxBytes * 8.0 / (flow->second.timeLastRxPacket.GetSeconds ()-flow->second.timeFirstTxPacket.GetSeconds ()) / 1000000  << " Mb/s" << std::endl;
          
          std::cout << "  Throughput:\t"   << flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds ()  << " Mb/s" << std::endl;
          std::cout << "  Mean delay:\t"   << (double)(flow->second.delaySum / (flow->second.rxPackets)).GetMicroSeconds () / 1000 << " ms" << std::endl;    
          if (flow->second.rxPackets > 1) {
            stationInfoVec[counter].Mean_Jitter = (double)(flow->second.jitterSum / (flow->second.rxPackets - 1)).GetMicroSeconds () / 1000;
            
            std::cout << "  Mean jitter:\t"  << (double)(flow->second.jitterSum / (flow->second.rxPackets - 1)).GetMicroSeconds () / 1000 << " ms" << std::endl;
          }    
          else{
            stationInfoVec[counter].Mean_Jitter = 0;
            std::cout << "  Mean jitter:\t---"   << std::endl;
          }
        }
      else
        {
          stationInfoVec[counter].Throughput = 0;
          stationInfoVec[counter].Mean_Delay = 0;
          stationInfoVec[counter].Mean_Jitter = 0;
          std::cout << "  Throughput:\t0 Mb/s" << std::endl;
          std::cout << "  Mean delay:\t---"    << std::endl;    
          std::cout << "  Mean jitter:\t---"   << std::endl;
        }

      uint16_t tid = t.destinationPort-1000;
      txBytesPerTid[tid]     += flow->second.txBytes;
      rxBytesPerTid[tid]     += flow->second.rxBytes;
      txPacketsPerTid[tid]   += flow->second.txPackets;
      rxPacketsPerTid[tid]   += flow->second.rxPackets;
      lostPacketsPerTid[tid] += flow->second.lostPackets;
      //throughputPerTid[tid]  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (flow->second.timeLastRxPacket.GetSeconds ()-flow->second.timeFirstTxPacket.GetSeconds ()) / 1000000 : 0);
      throughputPerTid[tid]  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds () : 0);
      delaySumPerTid[tid]    += flow->second.delaySum;
      jitterSumPerTid[tid]   += flow->second.jitterSum;

      txBytes     += flow->second.txBytes;
      rxBytes     += flow->second.rxBytes;
      txPackets   += flow->second.txPackets;
      rxPackets   += flow->second.rxPackets;
      lostPackets += flow->second.lostPackets;
      //throughput  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (flow->second.timeLastRxPacket.GetSeconds ()-flow->second.timeFirstTxPacket.GetSeconds ()) / 1000000 : 0);
      throughput  += (flow->second.rxPackets > 0 ? flow->second.rxBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds () : 0);
      delaySum    += flow->second.delaySum;
      jitterSum   += flow->second.jitterSum;
    }

  for (uint16_t tid = 0; tid < 8; tid++)
    if ((tid != 2) && (tid != 3) && (tid != 4) && (tid != 7))
      {
        std::cout << "=======================TID: " << tid << " =====================================" << std::endl;
        //per traffic category
        std::cout << "  Tx bytes:\t"     << txBytesPerTid[tid]     << std::endl;
        std::cout << "  Rx bytes:\t"     << rxBytesPerTid[tid]     << std::endl;
        std::cout << "  Tx packets:\t"   << txPacketsPerTid[tid]   << std::endl;
        std::cout << "  Rx packets:\t"   << rxPacketsPerTid[tid]   << std::endl;
        std::cout << "  Lost packets:\t" << lostPacketsPerTid[tid] << std::endl;
        std::cout << "  Throughput:\t"   << throughputPerTid[tid]  << " Mb/s" << std::endl;
        if (rxPacketsPerTid[tid] > 0)
          {
            std::cout << "  Mean delay:\t"   << (double)(delaySumPerTid[tid] / (rxPacketsPerTid[tid])).GetMicroSeconds () / 1000 << " ms" << std::endl;    
            if (rxPackets > 1)  
              std::cout << "  Mean jitter:\t"  << (double)(jitterSumPerTid[tid] / (rxPacketsPerTid[tid] - 1)).GetMicroSeconds () / 1000  << " ms" << std::endl;   
            else
              std::cout << "  Mean jitter:\t---"   << std::endl;
          }
        else
          {
            std::cout << "  Mean delay:\t---"    << std::endl;    
            std::cout << "  Mean jitter:\t---"   << std::endl;
          }
      }

  std::cout << "=======================Total: =====================================" << std::endl;

  std::cout << "  Rx bytes:\t"     << rxBytes     << std::endl;
  std::cout << "  Tx packets:\t"   << txPackets   << std::endl;
  std::cout << "  Rx packets:\t"   << rxPackets   << std::endl;
  std::cout << "  Lost packets:\t" << lostPackets << std::endl;
  std::cout << "  Tx bytes:\t"     << txBytes     << std::endl;
  std::cout << "  Throughput:\t"   << throughput  << " Mb/s" << std::endl;
  if (rxPackets > 0)
    {
      std::cout << "  Mean delay:\t"   << (double)(delaySum / (rxPackets)).GetMicroSeconds () / 1000 << " ms" << std::endl;    
      if (rxPackets > 1)  
        std::cout << "  Mean jitter:\t"  << (double)(jitterSum / (rxPackets - 1)).GetMicroSeconds () / 1000  << " ms" << std::endl;   
      else
        std::cout << "  Mean jitter:\t---"   << std::endl;
    }
  else
    {
      std::cout << "  Mean delay:\t---"    << std::endl;    
      std::cout << "  Mean jitter:\t---"   << std::endl;
    }


  std::cout << "=======================L2 Statistics: =====================================" << std::endl;
  
  for(uint32_t n=0; n < stationInfoVec.size(); n++) {
    StationInformation station = stationInfoVec[n];
    std::cout << "L2 for station number " << n << std::endl;
    // std::cout << "\tStation Node: " <<  station.staNode << std::endl;
    std::cout << "\tMAC Address: " << station.staMacAddress << std::endl;
    std::cout << "\tIP Address: " << station.staAddress << std::endl;
    // std::cout << "\tmsdusTxed: " << station.msdusTxed << std::endl;
    // std::cout << "\tamsdusTxed: " << station.amsdusTxed <<  std::endl;
    // std::cout << "\tampdusTxed: " << station.ampdusTxed << std::endl;
    // std::cout << "\tmsdusRetry: " << station.msdusRetry << std::endl;
    // std::cout << "\tamsdusRetry: " << station.amsdusRetry << std::endl;
    // std::cout << "\trxErrors: " << station.rxErrors << std::endl;
    // std::cout << "\tackRxErrors: " << station.ackRxErrors << std::endl;
    // std::cout << "\tampdusRxed: " << station.ampdusRxed << std::endl;
    // std::cout << "\tamsdusRxed: " << station.amsdusRxed << std::endl;
    // std::cout << "\tmsdusOrig: " << station.msdusOrig << std::endl;
    // std::cout << "\tmsduTxFails: " << station.msduTxFails << std::endl;
    // std::cout << "\toverloadDrops: " << station.overloadDrops << std::endl;
    // std::cout << "\tlifetimeDrops: " << station.lifetimeDrops << std::endl;
    // std::cout << "\trecvBytes: " << station.recvBytes << std::endl;
    // std::cout << "\tl2m_lastACKFrameUID: " << station.L2m_lastACKFrameUID << std::endl;
    // std::cout << "\tl2m_lastDATAFrameUID: " << station.L2m_lastDATAFrameUID << std::endl;
    std::cout << "\tdelaySum: " << station.delaySum.GetSeconds() << " seconds"<< std::endl;
    std::cout << "\tjitterSum: " << station.jitterSum.GetSeconds() << " seconds"<< std::endl;
    std::cout << "\tL2delaySum: " << station.L2delaySum.GetSeconds() << " seconds"<< std::endl;
    std::cout << "\tL2jitterSum: " << station.L2jitterSum.GetSeconds() << " seconds"<< std::endl;
    // std::cout << "\n\tThroughput: " << station.recvBytes * 8.0 / (simulationTime - Seconds (calcStart)).GetMicroSeconds () << std::endl;
    // std::cout << "\tMean Delay: " << ( (station.msdusRxed > 0)
    //                                   ? ( (double)(station.delaySum / station.msdusRxed).GetMicroSeconds () / 1000)
    //                                   : 0.0 ) << std::endl;
    // std::cout << "\tMean Jitter: " << ( (station.msdusRxed > 0)
    //                                   ? ( (double)(station.jitterSum / station.msdusRxed).GetMicroSeconds () / 1000)
    //                                   : 0.0 ) << std::endl;

    std::cout << "\tThroughput: " << station.Throughput << std::endl;
    std::cout << "\tMean Delay: " << station.Mean_Delay << " ms" <<  std::endl;
    std::cout << "\tMean Jitter: " << station.Mean_Jitter << " ms" << std::endl;

    std::cout << "\tL2 Mean Delay: " << ( (station.L2msdusRxed > 0)
                                      ? ( (double)(station.L2delaySum / station.L2msdusRxed).GetMicroSeconds () / 1000)
                                      : 0.0 ) << " ms" << std::endl;
    std::cout << "\tL2 Mean Jitter: " << ( (station.msdusRxed > 0)
                                      ? ( (double)(station.L2jitterSum / station.L2msdusRxed).GetMicroSeconds () / 1000)
                                      : 0.0 ) << " ms" << std::endl;
    
  }

  writeStatisticData(directory, filepath, 1);

  return 0;
}
