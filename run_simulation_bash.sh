#!/bin/bash

for (( i=1; $i <= 25; i++ )) ; do
    factor=2  
    c=$(( $i * $factor))
    echo "--------------------------"
    echo "Badany throughput $c"
    ./waf --run "scenario_8_6max_54_be_vo --VO=1 --VI=0 --BE=1 --BK=0 --Mbps=$c --packetSize=1470 --calcStart=5 --simTime=25"    
    echo "--------------------------"
done
